// requires...
const fs = require('fs');
const path = require('path');
const fsp = require('fs').promises;

// constants...
const mainDir = 'files/';

function createFile(req, res, next) {
	if (!req.body.filename) {
		res.status(400).send({ 'message': "Please specify 'filename' parameter" });
	}
	if (!req.body.filename.split('.').length) {
		res.status(400).send({ 'message': "Please specify 'filename' parameter" });
	}
	if (!req.body.content) {
		res.status(400).send({ 'message': "Please specify 'content' parameter" });
	}

	fs.writeFile(`${mainDir}${req.body.filename}`, req.body.content, () => {
		res.status(200).send({ 'message': 'File created successfully' });
	});
}

function getFiles(req, res, next) {
	fs.readdir(mainDir, (err, files) => {
		if (err) {
			res.status(400).send({ 'message': 'Client error' });
		}
		res.status(200).send({
			'message': 'Success',
			'files': files,
		});
	});
}

function getFile(req, res, next) {
	const filename = req.params.filename;

	fs.readFile(`${mainDir}/${filename}`, 'utf8', (err1, data) => {
		if (err1) {
			res
				.status(400)
				.send({ 'message': `No file with ${filename} filename found` });
		} else {
			const extension = path.extname(filename).slice(1);
			const stats = fs.statSync(`${mainDir}/${filename}`);
			const { mtime } = stats;

			res.status(200).send({
				'message': 'Success',
				'filename': filename,
				'content': data,
				'extension': extension,
				'uploadedDate': mtime,
			});
		}
	});
}

// Other functions - editFile, deleteFile //

module.exports = {
	createFile,
	getFiles,
	getFile,
};
